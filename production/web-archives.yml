---
apiVersion: apps/v1
kind: Deployment
metadata:
  name: web-archives
  labels:
    name: web-archives
    owner: agepinfo
spec:
  replicas: 1
  selector:
    matchLabels:
      app: web-archives
      owner: agepinfo
  strategy:
    type: RollingUpdate
    rollingUpdate:
      maxSurge: 2
      maxUnavailable: 0
  template:
    metadata:
      labels:
        app: web-archives
        owner: agepinfo
    spec:
      imagePullSecrets:
        - name: gitlab-kubernetes
        - name: gitlab-dckreg
      containers:
        - name: web-archives
          image: registry.gitlab.com/agepoly/it/infra/kubernetes/web-archives:665b6dd2
          ports:
            - containerPort: 80
          env:
            - name: HOST
              value: "web-archives.agepoly.ch"
          volumeMounts:
            - {"name":"web-archives-data-v","mountPath":"/Collect/Collect/public/s"}
            - {"subPath":"config.json","name":"web-archives-secrets--collect-collect-config-json","mountPath":"/Collect/Collect/config.json"}
          livenessProbe:
            failureThreshold: 2
            httpGet:
              path: /public/list
              port: 80
              scheme: HTTP
              httpHeaders:
                - name: Host
                  value: web-archives.agepoly.ch
            initialDelaySeconds: 300
            periodSeconds: 300
            timeoutSeconds: 10
          readinessProbe:
            failureThreshold: 1
            httpGet:
              path: /public/list
              port: 80
              scheme: HTTP
              httpHeaders:
                - name: Host
                  value: web-archives.agepoly.ch
            initialDelaySeconds: 5
            periodSeconds: 150
            timeoutSeconds: 10
          startupProbe:
            failureThreshold: 30
            httpGet:
              path: /public/list
              port: 80
              scheme: HTTP
              httpHeaders:
                - name: Host
                  value: web-archives.agepoly.ch
            initialDelaySeconds: 5
            periodSeconds: 10
            timeoutSeconds: 10
      nodeSelector:
        storage_node: "true"
      volumes:
        - {"persistentVolumeClaim":{"claimName":"web-archives-data-pvc"},"name":"web-archives-data-v"}
        - {"secret":{"defaultMode":292,"items":[{"path":"config.json","key":"-collect-collect-config-json"}],"secretName":"web-archives-file-secrets"},"name":"web-archives-secrets--collect-collect-config-json"}

---
apiVersion: v1
kind: Service
metadata:
  name: web-archives
  labels:
    name: web-archives
    owner: agepinfo
spec:
  ports:
    - port: 80
      targetPort: 80
      protocol: TCP
      name: http
  selector:
      app: web-archives
      owner: agepinfo
  type: ClusterIP

---
apiVersion: traefik.containo.us/v1alpha1
kind: IngressRoute
metadata:
  name: web-archives-main-domain
  labels:
    name: web-archives
    owner: agepinfo
spec:
  entryPoints:
    - websecure
  tls:
    secretName: agepoly.ch-wildcard-ssl
  routes:
    - match: Host(`web-archives.agepoly.ch`)
      kind: Rule
      services:
        - name: web-archives
          port: 80
          sticky:
            cookie:
              httpOnly: true
              name: lb
              secure: true
              sameSite: none
          strategy: RoundRobin

---
kind: PersistentVolumeClaim
apiVersion: v1
metadata:
  name: web-archives-data-pvc
spec:
  accessModes:
    - ReadWriteOnce
  resources:
    requests:
      storage: 20G
