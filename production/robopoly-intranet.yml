---
apiVersion: apps/v1
kind: Deployment
metadata:
  name: robopoly-intranet
  labels:
    name: robopoly-intranet
    owner: robopoly
spec:
  replicas: 1
  selector:
    matchLabels:
      app: robopoly-intranet
      owner: robopoly
  strategy:
    type: RollingUpdate
    rollingUpdate:
      maxSurge: 2
      maxUnavailable: 0
  template:
    metadata:
      labels:
        app: robopoly-intranet
        owner: robopoly
    spec:
      imagePullSecrets:
        - name: gitlab-kubernetes
        - name: gitlab-dckreg
      containers:
        - name: robopoly-intranet
          image: registry.gitlab.com/agepoly/it/infra/kubernetes/robopoly-intranet:665b6dd2
          ports:
            - containerPort: 8080
          env:
            - name: SECRET_KEY
              valueFrom:
                secretKeyRef:
                  name: robopoly-intranet-env-secrets
                  key: SECRET_KEY
            - name: DB_NAME
              valueFrom:
                secretKeyRef:
                  name: robopoly-intranet-env-secrets
                  key: DB_NAME
            - name: DB_USER
              valueFrom:
                secretKeyRef:
                  name: robopoly-intranet-env-secrets
                  key: DB_USER
            - name: DB_PASSWORD
              valueFrom:
                secretKeyRef:
                  name: robopoly-intranet-env-secrets
                  key: DB_PASSWORD
            - name: DB_ENGINE
              value: "mysql"
            - name: DB_HOST
              value: "databases"
            - name: DB_PORT
              value: "3306"
            - name: HOST
              value: "robopoly-intranet.agepoly.ch:intranet.robopoly.ch"
          livenessProbe:
            failureThreshold: 2
            httpGet:
              path: /admin
              port: 8080
              scheme: HTTP
              httpHeaders:
                - name: Host
                  value: robopoly-intranet.agepoly.ch
            initialDelaySeconds: 300
            periodSeconds: 300
            timeoutSeconds: 10
          readinessProbe:
            failureThreshold: 1
            httpGet:
              path: /admin
              port: 8080
              scheme: HTTP
              httpHeaders:
                - name: Host
                  value: robopoly-intranet.agepoly.ch
            initialDelaySeconds: 5
            periodSeconds: 150
            timeoutSeconds: 10
          startupProbe:
            failureThreshold: 30
            httpGet:
              path: /admin
              port: 8080
              scheme: HTTP
              httpHeaders:
                - name: Host
                  value: robopoly-intranet.agepoly.ch
            initialDelaySeconds: 5
            periodSeconds: 10
            timeoutSeconds: 10

---
apiVersion: v1
kind: Service
metadata:
  name: robopoly-intranet
  labels:
    name: robopoly-intranet
    owner: robopoly
spec:
  ports:
    - port: 80
      targetPort: 8080
      protocol: TCP
      name: http
  selector:
      app: robopoly-intranet
      owner: robopoly
  type: ClusterIP

---
apiVersion: traefik.containo.us/v1alpha1
kind: IngressRoute
metadata:
  name: robopoly-intranet-main-domain
  labels:
    name: robopoly-intranet
    owner: robopoly
spec:
  entryPoints:
    - websecure
  tls:
    secretName: agepoly.ch-wildcard-ssl
  routes:
    - match: Host(`robopoly-intranet.agepoly.ch`)
      kind: Rule
      services:
        - name: robopoly-intranet
          port: 80
          sticky:
            cookie:
              httpOnly: true
              name: lb
              secure: true
              sameSite: none
          strategy: RoundRobin

---
apiVersion: traefik.containo.us/v1alpha1
kind: IngressRoute
metadata:
  name: robopoly-intranet-intranet.robopoly.ch
  labels:
    name: robopoly-intranet
    owner: robopoly
spec:
  entryPoints:
    - websecure
  tls:
    secretName: intranet.robopoly.ch-cert
  routes:
    - match: Host(`intranet.robopoly.ch`)
      kind: Rule
      services:
        - name: robopoly-intranet
          port: 80
          sticky:
            cookie:
              httpOnly: true
              name: lb
              secure: true
              sameSite: none
          strategy: RoundRobin

---
apiVersion: cert-manager.io/v1
kind: Certificate
metadata:
  name: intranet.robopoly.ch-cert
  labels:
    name: robopoly-intranet
    owner: robopoly
spec:
  commonName: intranet.robopoly.ch
  secretName: intranet.robopoly.ch-cert
  dnsNames:
    - intranet.robopoly.ch
  issuerRef:
    name: letsencrypt-http
    kind: Issuer

