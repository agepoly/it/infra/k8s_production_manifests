<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML><HEAD><TITLE><?php echo $_GET['nom'] ?> - <?php echo $_GET['poste'] ?></TITLE>
<META content="text/html; charset=unicode" http-equiv=Content-Type>
<BODY>
<?php if (!isset( $_GET['nom'] )) { ?>

<form action="./">
  Prénom Nom (ex: Jean Dupont)<br>
  <input type="text" name="nom"><br>
  Poste (ex: Responsable Services)<br>
  <input type="text" name="poste"><br>
  Téléphone Privé [avec le +41 ou +33 et des espaces] (ex: +41 78 111 11 11)<br>
  <input type="text" name="tel"><br>
  <input type="submit" value="Generez">
</form>
<?php } else { ?>

<TABLE cellSpacing=0 cellPadding=1 border=0 style="margin-top:15px">

    <!-- cellule de gauche (image convertie en base64 pour éviter de devoir l'héberger) -->
    <TD align=middle vAlign=middle style="padding-top: 0px; padding-left: 0px; padding-right: 8px;">
        <IMG style="height: 95px" src="https://agepoly.ch/signature/logo.png" alt="logo agepoly"/>
    </TD>
    <!-- cellule de droite -->
    <TD style="font-size: 9pt; font-family: Calibri; padding-top: 0px; padding-left: 8px; border-left: #bbd0d5 1px solid" vAlign=top>

        <div style="font-size: 11pt; padding-bottom:6px">

            <!-- REMPLIR ICI --> <STRONG style="color: #1073a0"> <?php echo $_GET['nom'] ?></STRONG>
            <SPAN style="color: #bbd0d5"> | </SPAN>
            <!-- REMPLIR ICI --> <STRONG style="color: #000000"> <?php echo $_GET['poste'] ?></STRONG><BR>
        </div>

        <div style="padding-bottom:6px;">
            <STRONG style="color: #1073a0">AGEPoly - </STRONG>
            <SPAN style="color: #000000">Association Générale des Etudiant.e.s de l'EPFL</SPAN><BR>
        </div>

        <!-- REMPLIR ICI --> <STRONG style="color: #1073a0">privé :</STRONG><SPAN style="color: #000000"> <?php echo $_GET['tel'] ?></SPAN><BR>
        <STRONG style="color: #1073a0">secrétariat :</STRONG><SPAN style="color: #000000"> +41 21 693 20 95</SPAN><BR>
        <STRONG style="color: #1073a0">adresse :</STRONG><SPAN style="color: #000000"> Esplanade | Station 9 | CH-1015 Lausanne</SPAN>
    </TABLE>
    
<?php } ?> 
</BR>
</BODY></HTML>
