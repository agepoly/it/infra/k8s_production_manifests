---
apiVersion: apps/v1
kind: Deployment
metadata:
  name: esnwiki
  labels:
    name: esnwiki
    owner: esn
spec:
  replicas: 1
  selector:
    matchLabels:
      app: esnwiki
      owner: esn
  strategy:
    type: RollingUpdate
    rollingUpdate:
      maxSurge: 2
      maxUnavailable: 0
  template:
    metadata:
      labels:
        app: esnwiki
        owner: esn
    spec:
      imagePullSecrets:
        - name: gitlab-kubernetes
        - name: gitlab-dckreg
      containers:
        - name: esnwiki
          image: registry.gitlab.com/agepoly/it/infra/kubernetes/esnwiki:665b6dd2
          ports:
            - containerPort: 80
          env:
            - name: DB_HOST
              valueFrom:
                secretKeyRef:
                  name: esnwiki-env-secrets
                  key: DB_HOST
            - name: DB_USER
              valueFrom:
                secretKeyRef:
                  name: esnwiki-env-secrets
                  key: DB_USER
            - name: DB_NAME
              valueFrom:
                secretKeyRef:
                  name: esnwiki-env-secrets
                  key: DB_NAME
            - name: DB_PASSWORD
              valueFrom:
                secretKeyRef:
                  name: esnwiki-env-secrets
                  key: DB_PASSWORD
            - name: HOST
              value: "esnwiki.agepoly.ch"
          volumeMounts:
            - {"name":"esnwiki-wiki-uploads-v","mountPath":"/var/www/html/uploads"}
          livenessProbe:
            failureThreshold: 2
            httpGet:
              path: /
              port: 80
              scheme: HTTP
              httpHeaders:
                - name: Host
                  value: esnwiki.agepoly.ch
            initialDelaySeconds: 300
            periodSeconds: 300
            timeoutSeconds: 10
          readinessProbe:
            failureThreshold: 1
            httpGet:
              path: /
              port: 80
              scheme: HTTP
              httpHeaders:
                - name: Host
                  value: esnwiki.agepoly.ch
            initialDelaySeconds: 5
            periodSeconds: 150
            timeoutSeconds: 10
          startupProbe:
            failureThreshold: 30
            httpGet:
              path: /
              port: 80
              scheme: HTTP
              httpHeaders:
                - name: Host
                  value: esnwiki.agepoly.ch
            initialDelaySeconds: 5
            periodSeconds: 10
            timeoutSeconds: 10
      nodeSelector:
        storage_node: "true"
      volumes:
        - {"persistentVolumeClaim":{"claimName":"esnwiki-wiki-uploads-pvc"},"name":"esnwiki-wiki-uploads-v"}

---
apiVersion: v1
kind: Service
metadata:
  name: esnwiki
  labels:
    name: esnwiki
    owner: esn
spec:
  ports:
    - port: 80
      targetPort: 80
      protocol: TCP
      name: http
  selector:
      app: esnwiki
      owner: esn
  type: ClusterIP

---
apiVersion: traefik.containo.us/v1alpha1
kind: IngressRoute
metadata:
  name: esnwiki-main-domain
  labels:
    name: esnwiki
    owner: esn
spec:
  entryPoints:
    - websecure
  tls:
    secretName: agepoly.ch-wildcard-ssl
  routes:
    - match: Host(`esnwiki.agepoly.ch`)
      kind: Rule
      services:
        - name: esnwiki
          port: 80
          sticky:
            cookie:
              httpOnly: true
              name: lb
              secure: true
              sameSite: none
          strategy: RoundRobin

---
kind: PersistentVolumeClaim
apiVersion: v1
metadata:
  name: esnwiki-wiki-uploads-pvc
spec:
  accessModes:
    - ReadWriteOnce
  resources:
    requests:
      storage: 15G
