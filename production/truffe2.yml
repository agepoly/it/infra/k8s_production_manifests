---
apiVersion: apps/v1
kind: Deployment
metadata:
  name: truffe2
  labels:
    name: truffe2
    owner: agepinfo
spec:
  replicas: 1
  selector:
    matchLabels:
      app: truffe2
      owner: agepinfo
  strategy:
    type: RollingUpdate
    rollingUpdate:
      maxSurge: 2
      maxUnavailable: 0
  template:
    metadata:
      labels:
        app: truffe2
        owner: agepinfo
    spec:
      imagePullSecrets:
        - name: gitlab-kubernetes
        - name: gitlab-dckreg
      initContainers:
        - name: init-container
          image: gitlab.com/agepoly/dependency_proxy/containers/busybox
          command: ['sh', '-c', 'chown -R 33:33 /var/www/truffe2-git/truffe2/media/uploads/']
          volumeMounts:
            - {"name":"truffe2-uploads-v","mountPath":"/var/www/truffe2-git/truffe2/media/uploads/"}
            - {"subPath":"config.yaml","name":"truffe2-secrets--etc-truffe2-config-yaml","mountPath":"/etc/truffe2/config.yaml"}
            - {"subPath":"id_rsa","name":"truffe2-secrets--root--ssh-id-rsa","mountPath":"/root/.ssh/id_rsa"}
      containers:
        - name: truffe2
          image: registry.gitlab.com/agepoly/it/infra/kubernetes/truffe2:665b6dd2
          ports:
            - containerPort: 80
          env:
            - name: APP_CONFIG
              value: "/etc/truffe2/config.yaml"
            - name: HOST
              value: "truffe2.agepoly.ch"
          volumeMounts:
            - {"name":"truffe2-uploads-v","mountPath":"/var/www/truffe2-git/truffe2/media/uploads/"}
            - {"subPath":"config.yaml","name":"truffe2-secrets--etc-truffe2-config-yaml","mountPath":"/etc/truffe2/config.yaml"}
            - {"subPath":"id_rsa","name":"truffe2-secrets--root--ssh-id-rsa","mountPath":"/root/.ssh/id_rsa"}
          livenessProbe:
            failureThreshold: 2
            httpGet:
              path: /
              port: 80
              scheme: HTTP
              httpHeaders:
                - name: Host
                  value: truffe2.agepoly.ch
            initialDelaySeconds: 300
            periodSeconds: 300
            timeoutSeconds: 10
          readinessProbe:
            failureThreshold: 1
            httpGet:
              path: /
              port: 80
              scheme: HTTP
              httpHeaders:
                - name: Host
                  value: truffe2.agepoly.ch
            initialDelaySeconds: 5
            periodSeconds: 150
            timeoutSeconds: 10
          startupProbe:
            failureThreshold: 30
            httpGet:
              path: /
              port: 80
              scheme: HTTP
              httpHeaders:
                - name: Host
                  value: truffe2.agepoly.ch
            initialDelaySeconds: 5
            periodSeconds: 10
            timeoutSeconds: 10
      nodeSelector:
        storage_node: "true"
      volumes:
        - {"persistentVolumeClaim":{"claimName":"truffe2-uploads-pvc"},"name":"truffe2-uploads-v"}
        - {"secret":{"defaultMode":292,"items":[{"path":"config.yaml","key":"-etc-truffe2-config-yaml"}],"secretName":"truffe2-file-secrets"},"name":"truffe2-secrets--etc-truffe2-config-yaml"}
        - {"secret":{"defaultMode":256,"items":[{"path":"id_rsa","key":"-root--ssh-id-rsa"}],"secretName":"truffe2-file-secrets"},"name":"truffe2-secrets--root--ssh-id-rsa"}

---
apiVersion: v1
kind: Service
metadata:
  name: truffe2
  labels:
    name: truffe2
    owner: agepinfo
spec:
  ports:
    - port: 80
      targetPort: 80
      protocol: TCP
      name: http
  selector:
      app: truffe2
      owner: agepinfo
  type: ClusterIP

---
apiVersion: traefik.containo.us/v1alpha1
kind: IngressRoute
metadata:
  name: truffe2-main-domain
  labels:
    name: truffe2
    owner: agepinfo
spec:
  entryPoints:
    - websecure
  tls:
    secretName: agepoly.ch-wildcard-ssl
  routes:
    - match: Host(`truffe2.agepoly.ch`)
      kind: Rule
      services:
        - name: truffe2
          port: 80
          sticky:
            cookie:
              httpOnly: true
              name: lb
              secure: true
              sameSite: none
          strategy: RoundRobin

---
kind: PersistentVolumeClaim
apiVersion: v1
metadata:
  name: truffe2-uploads-pvc
spec:
  accessModes:
    - ReadWriteOnce
  resources:
    requests:
      storage: 20G
